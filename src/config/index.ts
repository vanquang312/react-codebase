import * as Axios from "axios"

export const axiosClient = Axios.default.create({
  baseURL: "http://localhost:8000",
  responseType: "json"
})
