import { routersNotAuth } from "@routers/index"
import React, { lazy, Suspense } from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { LoadingLazyPage } from "@components/index"
import NotFound from "../not-found"
import Layout from "@/components/layout"

const Components = {}

routersNotAuth.forEach(route => {
  Components[route.component] = lazy(() => import(`@pages/${route.component}`))
})

const AppRouter = props => {
  return (
    <Router
      basename={
        process.env.APP_BASENAME_PATH
          ? `${process.env.APP_BASENAME_PATH}/`
          : "/"
      }
    >
      <Layout>
        <Suspense fallback={<LoadingLazyPage />}>
          <Switch>
            {routersNotAuth.map((route, index) => (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                render={routeProps => {
                  const Component = Components[route.component]
                  return <Component {...props} {...routeProps} route={route} />
                }}
              />
            ))}
            <Route path="/*">
              <NotFound />
            </Route>
          </Switch>
        </Suspense>
      </Layout>
    </Router>
  )
}

export default AppRouter
