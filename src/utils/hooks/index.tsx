import { TAuthContext } from "@common/index"
import { AuthContext } from "@context/index"
import { useContext } from "react"

export const useAuth = () => useContext<TAuthContext>(AuthContext)
